## Usage

**Install**:

```bash
$ npm i --save-dev equ-prettier-config
```

**Edit `package.json`**:

```jsonc
{
    // ...
    "prettier": "equ-prettier-config"
}
```
